# Pub/Sub / Chain of Responsiblity 

## Why

​**问题**：

如何划分责任​

**方案**：

    Pub/Sub
    

    Chain of Responsiblity

    Mediator

    


## How

* ​     语法实现 (Java Code)
* ​     框架实现 (Lambok)
* ​     语言实现 (scala case class)

## 优缺点

    Pub/Sub Oberserver 多年演变下来之后变成了 Rx 库，这个模式在早期

## 讨论

结合 DI 讨论适应场景
