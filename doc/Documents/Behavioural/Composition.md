# Strategy / Template Method / Mediator

## Why

​**问题**：

如何归并功能, OOP/OOD 设计中经常强调高内聚，松耦合, 内涵在于对象行为各异，但是在某些场合又要把某些相关的内容关联在一起
Behavioural 类的整合模式和 Structural 类的 侧重点要分辨开

**方案**：

    Template Method
            
    Strategy


## How

* ​     语法实现 (Java Code)
* ​     框架实现 (Lambok)
* ​     语言实现 (scala case class)

## 优缺点

​
虽然用Builder Class 减少了多重构造器编写，但是同样增加了代码复杂度，而且无效依赖的问题无法根治

## 讨论

结合 DI 讨论适应场景
