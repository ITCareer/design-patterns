# Factory Method / Abstract Factory Pattern

## Why

**问题**: 需要生成各类子类型对象，应对具体使用空间，但是每个调用处使用 new 则带来更多新问题，（结合 object pool，singleton 等其它模式讨论），重点在于辅助OOP的继承多态管理。
​
构造复杂对象时，需要组合不同的的原始参数构造，目前C++，Java等主流OOP语言的构造器函数在表达形式上无法灵活表达（需要使用多个构造函数对应需求）

**方案**：统一抽象，隐藏生成细节

 ​      Factory Method：（注：概念上容易和Builder Pattern 混肴）通过*函数*统一生成出入口，根据函数输入*参数*处理生成逻辑
 ​      Abstract Factory Pattern：通过定义*接口*统一抽象，由接口类型的*具体子类*处理生成逻辑
       Builder：为有需要的复杂对象另外编写一个Builder Class.

## How

* ​     语法实现 (见例子)
* ​     框架实现 (无)
* ​     语言实现 (无)

## 优缺点

Factory Method 需要的 if else 和 switch 做类型判断，导致增加新类型时候必须匹配对应的参数，函数入口会变得越来越臃肿，好处是single source of truth，坏处是团队作战时修改时会有机会出现冲突

Abstract Factory Pattern 解决了对一个函数产生过多依赖的问题，但是在阅读代码时无法得知实现绑定是否合理，增加了代码维护成本

虽然用Builder Class 减少了多重构造器编写，但是同样增加了代码复杂度，而且无效依赖的问题无法根治

另：两个工厂模式都会通过返回提供产品类型的子类，但生成函数的声明必须以父类声明，导致获取生成的对象或许需要进行二次校验（注：需要在此提及Test Case 的重要性）

Builder 结合（或者隐藏在两种工程模式中

## 讨论

如果类型系统是参数型多态是否能避免相关陷阱？



## 讨论

