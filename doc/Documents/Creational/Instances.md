# Object Pool / Prototype / Singleton(Multiton) / RAII* (需要C++背景 )

## Why

​**问题**：在OOP 里面，Class是蓝图，Object/Instance是具体对象，如何规范大型程序中instance的使用（引用）及其生命周期管理
​

**方案**：统一抽象，隐藏生成细节

* ​      Object Pool: Instance复用，减少Instance生成及释放时带来的性能开销
* ​      Protype: 同一Class的多相同instnace，目的为确保行为完全一致 （注，提及 Java的 cloneable 和 Scala的 copy，另外JS 的Prototype）
* ​      Singleton: 单体Instance，全局系统内一个Class蓝图应该有且只有一个 Instance
* ​      Multiton: 多个相关Instance的Singleton，只是强调了几个Singleton间的相关性

## How

* ​     语法实现 (Java enum)
* ​     框架实现 (Spring,Guava,Apache Common and etc.)
* ​     语言实现 (Scala object, case class)

## 优缺点

Instance 的资源管理由于不同语言的运行生态不一，这个模式的通用性反而最难控制，例如 Generic 和非 Generic的 实现差异，多线程的系列里面的一致性考虑。

这一章反而是最需要列框架例子而不应该使用代码做演示

## 讨论

这几个模式在资源控制和多线程系统中的选型侧重，语言本身带来的局限是否比模式更重要？
