# Flyweight / Proxy

## Why

​**问题**：

功能相同的模块占用过多的资源，甚至对于客户（使用者），这种资源消耗会影响使用


**方案**：

控制模块属性不可变

## How

* ​     语法实现 (Java Code)
* ​     框架实现 (Lambok)
* ​     语言实现 (scala case class)

## 优缺点

​

## 讨论

结合 DI 讨论适应场景
