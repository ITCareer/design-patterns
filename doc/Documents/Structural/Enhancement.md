# Bridge / Adapter / Decorator 

## Why

​**问题**：

复杂的系统中，各个组件或者模块都相对独立，但是在某些时候需要使用或者关联某一个功能模块，重新开发类似功能的替代品成本太高，需要有方式使用相关模型。

**方案**：

    Adapter: 设计思路来源于现实生活中的转换器，Adapter模式处理的是把两个不同接口的模块联通
    Bridge: 拆分抽象和实现，并留在运行时绑定两者

    这两个方案是最容易混肴的，区别在于处理的具体问题不一样

    Decorator: 在不改动原有的模块为原有的模块增加新
    Facade: 



## How

* ​     语法实现 (Java Code)
* ​     框架实现 (Lambok)
* ​     语言实现 (scala case class)

## 优缺点

​
虽然用Builder Class 减少了多重构造器编写，但是同样增加了代码复杂度，而且无效依赖的问题无法根治

## 讨论

结合 DI 讨论适应场景
