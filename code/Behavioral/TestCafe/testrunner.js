const createTestCafe = require('./src/page/components/node_modules/testcafe');

createTestCafe('localhost', 1337, 1338)
            .then(tc => tc
            .createRunner()
            .src(["index.js"])
            .browsers(['chrome'])
            //.concurrency(3)
            //.reporter('allure')
            .screenshots({
                path: 'artifacts/screenshots',
                takeOnFails: true
            })
            //.video('./report/videos/', {
            //    singleFile: true,
            //    failedOnly: true
            //})
            .run({
                  skipJsErrors: true,
                  //quarantineMode: true,
                  selectorTimeout: 50000,
                  assertionTimeout: 7000,
                  pageLoadTimeout: 8000,
                  speed: 0.9,
                  //stopOnFirstFail: true
                })
    )
    .then(failedCount => {
        console.log('Tests failed: ' + failedCount);
        testcafe.close();
    })
