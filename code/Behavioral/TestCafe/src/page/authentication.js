import { Selector, t } from './components/node_modules/testcafe';

export class Page {
    
    constructor (userElememtId, passwordElementId, submitButtonName) {
        this.nameInput      = Selector(userElememtId);
        this.passwordInput  = Selector(passwordElementId);
        this.submitButton = Selector("input[name='"+submitButtonName+"']");
    }

    async signInWith (name, password) {
        await t
            .typeText(this.nameInput, name)
            .typeText(this.passwordInput, password)
            .click(this.submitButton);
    }
}
