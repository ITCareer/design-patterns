import { Selector, t } from 'testcafe';

class Page {

    constructor () {
        this.links = {
            "orderYourTranscript":  Selector("a[href='/dash/order/consent.jsf?faces-redirect=true']"),
            "openYourProfile":  Selector('a').withText('View Your Profile'),
            "openYourOrderActivity":  Selector('a').withText('View Your Order Activity'),
            "updateYourContactInfo":   Selector('a').withText('Update Your Contact Information')
        }
    }


    async clickOn(link) {
        await t.click(this.links[link]);
    }

}

export default new Page();
