import { Selector, t } from 'testcafe';

class Page {
    
    constructor () {
      
        this.moveToListLink     = Selector("button[title='move to list >']");
        this.availableInstitution =  Selector('li');
        this.consentCheckbox = Selector('input[id="helpForm:consentCheckbox"]');
        this.sendTransLink   = Selector('a[id="helpForm:sendTrans"]');
    }

   

    async selectInstitute (name) {
        await t
            .click(this.availableInstitution.withAttribute('data-item-label', name))
            .click(this.moveToListLink)
    }

    async consentToSendTranscript () {
        await t
            .click(this.consentCheckbox)
            .click(this.sendTransLink);
    }
}

export default new Page();