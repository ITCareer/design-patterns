import { Selector, t } from 'testcafe';

class Page {

    constructor () {
         this.orderdetailList  = Selector('a');
         this.submitOrderButton = Selector('button[id="helpForm:completeContinueButton"]');
         this.confirmCheckbox   = Selector("input[id ='formOrderWizard:checkboxConfirm']");
         this.addOrderToCartButton   = Selector('span').withText('Add Order to Cart');
         this.orderNumber = Selector('table[class="jrPage"] > tbody > tr:nth-child(2) > td:nth-child(2) > span');
 
    }

    async findOrderDetailByNumber (orderNumber) {
        const getNumber = await this.orderdetailList.withText(orderNumber);
        return getNumber;
    }

    async addOrderToCart () {
        await t
            .click(this.confirmCheckbox)
            .click(this.addOrderToCartButton);
    }

    async submitOrder () {
        await t
            .click(this.submitOrderButton);
    }

    async getOrderNumber () {
        const orderValue = await this.orderNumber.with({ timeout: 10000 }).textContent; 
        return orderValue;
    }
}

export default new Page();
