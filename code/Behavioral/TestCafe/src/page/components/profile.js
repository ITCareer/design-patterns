import { Selector, t } from 'testcafe';

class Page {

    constructor () {
        this.penNumber     = Selector("span[id='helpForm:PEN']");
    }

    async getPenNumber () {
        const penNumberValue = await this.penNumber.innerText;
        return penNumberValue
    }

}

export default new Page();
