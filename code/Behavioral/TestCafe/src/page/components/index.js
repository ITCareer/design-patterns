
export default {
     dashboard: await import('./dashboard.js'),
     insitutions: await import('./insitutions.js.js.js'),
     consentform: await import('./consentform.js'),
     shpportingCart: await import('./shoppingCart.js'),
     profile: await import('./profile.js'),

      async creatTranscriptOrder(config) {
          await this.dashboard.orderYourTranscript();
          await this.consentform.consentToSendTranscript ();
          await this.insitutions.select (config.institution);
      },

      async placeOrder () {
          await this.shoppingCart.addOrderToCart();
          await this.shoppingCart.submitOrder();
          const orderNumber = await this.shoppingCart.getOrderNumber ();
          return orderNumber
      }

     //  async selectInstitute (name) {
     //      await t
     //          .click(this.availableInstitution.withAttribute('data-item-label', name))
     //          .click(this.moveToListLink)
     //  }
  
};
