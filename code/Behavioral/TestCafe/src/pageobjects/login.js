import { Selector, t } from '../page/components/node_modules/testcafe';

class Page {

    constructor () {
        this.nameInput      = Selector('#user');
        this.passwordInput  = Selector('#password');
        this.continueButton = Selector("input[name='btnSubmit']");
    }

    async signIn (name, password) {
        await t
            .typeText(this.nameInput, name)
            .typeText(this.passwordInput, password)
            .click(this.continueButton);
    }
}

export default new Page();