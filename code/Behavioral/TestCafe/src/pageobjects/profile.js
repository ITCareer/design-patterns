import { Selector, t } from 'testcafe';


class Page {

    constructor () {
        this.penNumber     = Selector("span[id='helpForm:PEN']");
    }

    async getPenNumber () {
        return this.penNumber.innerText;
    }
}

export default new Page();
