import { Selector, t } from 'testcafe';



class Page {

    constructor () {
        this.moveToListLink     = Selector("button[title='move to list >']");
        this.goToNextStepButton = Selector('span').withText('Go to Next Step');
        this.aa = Selector('li').withAttribute('data-item-label', name);
    }

    async selectInstitute (name) {
       
        await t
            .click(this.aa)
            .click(this.moveToListLink)
            .click(this.goToNextStepButton);
    }
}

export default new Page();