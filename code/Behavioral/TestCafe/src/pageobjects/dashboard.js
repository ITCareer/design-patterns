import { Selector, t } from 'testcafe';

class Page {

    constructor () {
        this.orderYourTranscript_Link = Selector("a[href='/dash/order/consent.jsf?faces-redirect=true']");
        this.viewYourProfile_Link = Selector('a').withText('View Your Profile');
        this.viewYourOrderActivity_Link = Selector('a').withText('View Your Order Activity');
        this.updateYourContactInformation_Link = Selector('a').withText('Update Your Contact Information');
    }

    async clickOrderYourTranscriptLink () {
        await t
            .click(this.orderYourTranscript_Link);
    }

    async clickViewYourProfileLink () {
        await t
            .click(this.viewYourProfile_Link);
    }

   async clickViewYourOrderActivityLink () {
        await t
            .click(this.viewYourOrderActivity_Link);
    }

    async clickUpdateYourContactInformationLink () {
        await t
            .click(this.updateYourContactInformation_Link);
    }

}

export default new Page();
