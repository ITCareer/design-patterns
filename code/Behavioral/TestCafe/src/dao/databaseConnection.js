let connection;
 var oracledb = require('./oracledb');

 (async function() {
 try{
    connection = await oracledb.getConnection({
         user : 'userName',
         password : 'password',
         connectString : 'url:1521/aaa.bbb'
    });
    console.log("Successfully connected to Oracle!")
 } catch(err) {
     console.log("Error: ", err);
   } finally {
     if (connection) {
       try {
         await connection.close();
       } catch(err) {
         console.log("Error when closing the database connection: ", err);
       }
     }
   }
 })()