import { Selector } from './src/page/components/node_modules/testcafe';
import { RequestLogger } from './src/page/components/node_modules/testcafe';

import environment from './environment.js';
import authentication from './src/page/authentication.js';
import pages from './src/page/components/index.js';
import navigation from './src/page/navigation.js';

const userData = require("./src/data/user.json");
const config = require('./config.json');

fixture ('All about StudentTranscripts')
    .meta('Test type', 'Smoke test')
    .page (environment.baseUrl)
    .beforeEach( async t => {
        await new authentication("#user","#password","btnSubmit").signInWith(config.userName, config.passWord);
    })
    .afterEach( async t => {
    //  await t.click('#delete-data');
    });


/*
* This test is driven by "config.json" file, looping depends on the uses there.
*/
test.skip
.meta('testID', 't-001')
(`Order transcript for user: ${config.userName}`, async t => {
    await pages.dashboard.clickOn("orderYourTranscript");
    await pages.consentform.consentToSendTranscript ();
    await pages.insitutions.select (config.institution);
    await pages.navigation.goToNext ();
    await pages.shoppingCart.addOrderToCart();
    await pages.shoppingCart.submitOrder();
    const orderNumber = await pages.shoppingCart.getOrderNumber ();
    await t.expect(orderNumber.length).eql(6);
    console.log("---------------. Order number is:" + orderNumber);
});

test.skip
.meta('testID', 't-001')
(`Order transcript for user: ${config.userName}`, async t => {
    await components.creatOrder("transacript",config);
    await navigation.goToNext ();
    const orderNumber = await components.placeOrder ();
    await t.expect(orderNumber.length).eql(6);
    console.log("---------------. Order number is:" + orderNumber);
});

/*
* This test is driven by "config.json" file, looping depends on the uses there.
*/
///*
test.skip
.meta('testID', 't-002')
(`View profile for user: ${config.userName} and verify pen is: ${config.pen}`, async t => {
    await components.dashboard.clickOn("openYourProfile");
    const penNumber = await components.profile.getPenNumber ();
    await t.expect(penNumber).eql(config.pen);
    console.log("---------------. Pen number is:" + penNumber);
});
//*/


/*
* This test view profile and verify Order number.
*/
test.meta('testID', 't-003')
(`View profile for user: ${config.userName} and verify pen is: ${config.pen}`, async t => {
    await components.dashboard.clickOn("openYourOrderActivity");
    const order = await components.shoppingCart.findOrderDetailByNumber ('905104');
    await t.expect(order.exists).ok();
    console.log("---------------. Find order number:" + lookingNumber);
});
