import { Selector } from './src/page/components/node_modules/testcafe';
import { RequestLogger } from './src/page/components/node_modules/testcafe';

import environment from './environment.js';

import login from './src/pages/login';
import dashboard from './src/pages/dashboard';
import consent from './src/pages/consent';
import selectInstitution from './src/pages/selectInstitution';
import chooseSendOption from './src/pages/chooseSendOption';
import addToCart from './src/pages/addToCart';
import shoppingCart from './src/pages/shoppingCart';
import myprofile from './src/pages/profile';
import myOrderActivity from './src/pages/orderActivity';

const userData = require("./src/data/user.json");
const config = require('./config.json');

fixture ('All about StudentTranscripts')
    .meta('Test type', 'Smoke test')
    .page (environment.baseUrl)
    .beforeEach( async t => {
        await login.signIn (config.userName, config.passWord);
    })
    .afterEach( async t => {
    //  await t.click('#delete-data');
    });


/*
* This test is driven by "config.json" file, looping depends on the uses there.
*/
test.skip
.meta('testID', 't-001')
(`Order transcript for user: ${config.userName}`, async t => {
    await dashboard.clickOrderYourTranscriptLink ();
    await consent.sendTrans ();
    await selectInstitution.selectInstitute (config.institution);
    await chooseSendOption.goToNext ();
    await addToCart.addOrderToCart();
    await shoppingCart.submitOrder ();

    const orderNumber = await shoppingCart.getOrderNumber ();
    await  t.expect(orderNumber.length).eql(6);
    console.log("---------------. Order number is:" + orderNumber);
});


/*
* This test is driven by "config.json" file, looping depends on the uses there.
*/
///*
test.skip
.meta('testID', 't-002')
(`View profile for user: ${config.userName} and verify pen is: ${config.pen}`, async t => {
    await dashboard.clickViewYourProfileLink ();

    const penNumber = await myprofile.getPenNumber ();
    await t.expect(penNumber).eql(config.pen);
    console.log("---------------. Pen number is:" + penNumber);
});
//*/


/*
* This test view profile and verify Order number.
*/
test.meta('testID', 't-003')
(`View profile for user: ${config.userName} and verify pen is: ${config.pen}`, async t => {
    const lookingNumber = '905104';

    await dashboard.clickViewYourOrderActivityLink ();

    const orderNumber = await myOrderActivity.findOrderNumber (lookingNumber);
    await t.expect((orderNumber).exists).ok();
    console.log("---------------. Find order number:" + lookingNumber);
});
