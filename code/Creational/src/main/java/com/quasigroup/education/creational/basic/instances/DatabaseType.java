package com.quasigroup.education.creational.basic.instances;

public enum DatabaseType {

    H2("h2",""), PG("postgres","");

    final private String name;
    final private String gender;

    DatabaseType(String name, String gender) {
        this.name = name;
        this.gender = gender;
    }

    private void execute(){}

    public String getName() {
        return name;
    }
}
