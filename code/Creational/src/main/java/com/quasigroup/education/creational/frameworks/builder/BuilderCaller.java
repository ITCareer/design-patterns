package com.quasigroup.education.creational.frameworks.builder;

final public class BuilderCaller {

    public DataSource buildH2Builder(){
        return DataSource.builder()
                .driver("org.h2.Drive")
                .type("H2")
                .build();
    }

    public DataSource buildPGBuilder(){
        return DataSource.builder()
                .driver("org.postgresql.Driver")
                .type("postgresql")
                .host("localhost")
                // .withPort(5432)
                .schemaName("testing")
                .build();
    }
}

