package com.quasigroup.education.creational.basic.factories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PGFactory implements AbstractFactory {

    @Override
    public Connection createDBConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:postgres://localhost:5432",
                "user",
                "password"
        );
    }

}
