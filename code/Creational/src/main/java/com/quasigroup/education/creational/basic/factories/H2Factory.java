package com.quasigroup.education.creational.basic.factories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2Factory implements AbstractFactory{

    @Override
    public Connection createDBConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:mem:test", "sa", "sa");
    }
}
