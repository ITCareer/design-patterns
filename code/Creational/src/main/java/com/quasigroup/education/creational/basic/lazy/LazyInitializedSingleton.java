package com.quasigroup.education.creational.basic.lazy;

import java.util.concurrent.locks.Lock;

public class LazyInitializedSingleton {
    private static LazyInitializedSingleton instance;

    private LazyInitializedSingleton(){}

    public static LazyInitializedSingleton getInstance(){
        if(instance == null){
            instance = new LazyInitializedSingleton();
        }
        return instance;
    }

    public static LazyInitializedSingleton getInstanceUsingDoubleLocking(){

        if(instance == null){
            synchronized (LazyInitializedSingleton.class) {
                if(instance == null){
                    instance = new LazyInitializedSingleton();
                }
            }
        }
        return instance;
    }
}
