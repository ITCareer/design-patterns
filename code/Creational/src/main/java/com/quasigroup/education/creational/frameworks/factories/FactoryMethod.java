package com.quasigroup.education.creational.frameworks.factories;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
final public class FactoryMethod {

    final static String PG = "Postgres";
    final static String H2 = "H2";

    public static Connection createDBConnection(String dbType) throws SQLException {
        switch (dbType) {
            case PG:
                return DriverManager.getConnection("jdbc:oracle:thin:@<hostname>:<port num>:<DB name>", "user", "password");
            case H2:
                return DriverManager.getConnection("jdbc:oracle:thin:@<hostname>:<port num>:<DB name>", "user", "password");
            default:
                throw new IllegalStateException("Unexpected value: " + dbType);
        }

    }

    public static Connection createDBConnectionByAbstractFactory(AbstractFactory factory) throws SQLException {
        return factory.createDBConnection();
    }
}
