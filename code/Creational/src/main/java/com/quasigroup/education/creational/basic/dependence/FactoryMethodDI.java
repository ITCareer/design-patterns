package com.quasigroup.education.creational.basic.dependence;

import com.quasigroup.education.creational.basic.factories.AbstractFactory;

import java.sql.Connection;
import java.sql.SQLException;

final public class FactoryMethodDI {
    private AbstractFactory factory;

    public FactoryMethodDI(AbstractFactory factory) {
        this.factory = factory;
    }

    public Connection getConnection() throws SQLException {
        return factory.createDBConnection();
    }

    public void setFactory(AbstractFactory factory) {
        this.factory = factory;
    }
}