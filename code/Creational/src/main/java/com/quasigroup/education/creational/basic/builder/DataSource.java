package com.quasigroup.education.creational.basic.builder;

import org.apache.logging.log4j.util.Strings;

class DataSource {
    private String host;
    private String type;
    private String driver;
    private String schemaName;
    private String location;
    private Integer portNumber;

    public void register() throws ClassNotFoundException {
        Class.forName(driver);
    }

    public String getUrl() {
        return "jdbc:"+type+":"+location+"/"+schemaName;
    }

    public DataSource(String driver, String type, String schemaName){
        this.driver = driver;
        this.type = type;
        this.schemaName = schemaName;
        this.location = "mem";
    }

    public DataSource(String driver, String type, String schemaName,String host){
        this.host = host;
        this.driver = driver;
        this.type = type;
        this.schemaName = schemaName;
        this.location =  "//"+host ;
    }
    public DataSource(String driver, String type,  String schemaName, String host, Integer port){
        this.host = host;
        this.driver = driver;
        this.type = type;
        this.schemaName = schemaName;
        this.portNumber = port;
        this.location =  "//"+host+":"+portNumber;
    }

    public static class Builder {
        private String host;
        private String type;
        private String driver;
        private String schemaName;
        private Integer portNumber;

        public Builder(String driver, String type, String schemaName) {
            this.driver = driver;
            this.type = type;
            this.schemaName = schemaName;
        }
        public Builder atHost(String host){
            this.host = host;
            return this;  //By returning the builder each time, we can create a fluent interface.
        }
        public Builder withPort(Integer port){
            this.portNumber = port;
            return this;
        }


        public DataSource build(){
            DataSource source = new DataSource();
            source.host = this.host;
            source.driver = this.driver;
            source.type = this.type;
            source.location = Strings.isNotEmpty(host) ? (null != portNumber ? "//"+host+":"+portNumber : "//"+host ): "mem";
            source.schemaName = this.schemaName;
            return source;
        }
    }

    private DataSource() {

    }

}
