package com.quasigroup.education.creational.frameworks;

import com.quasigroup.education.creational.frameworks.instances.Database;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Main.class, args);
        System.out.println("Application context initialized!!!");
        Database.Type type1 = ctx.getBean("h2", Database.Type.class);
        System.out.println(type1.getName());

        Database.Type type2 = ctx.getBean("pg", Database.Type.class);
        System.out.println(type2.getName());
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            Arrays.stream(beanNames).forEach(System.out::println);
        };
    }

}