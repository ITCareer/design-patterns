package com.quasigroup.education.creational.frameworks.instances;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

public class Database {
    public static abstract class Type {
        private final String name;

        Type(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static class H2 extends Type {
        H2() {
            super("h2");
        }
    }

    public static class PG extends Type {
        PG() {
            super("postgres");
        }
    }


    @Bean("h2")
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public Type getH2() {
        return new H2();
    }

    @Bean("pg")
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public Type getPG() {
        return new PG();
    }

}
