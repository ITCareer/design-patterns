package com.quasigroup.education.creational.frameworks.dependence;

import com.quasigroup.education.creational.basic.factories.AbstractFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;

@Component
final public class FactoryMethodDI {

    @Autowired
     private AbstractFactory factory;

    public Connection getConnection() throws SQLException {
        return factory.createDBConnection();
    }

}