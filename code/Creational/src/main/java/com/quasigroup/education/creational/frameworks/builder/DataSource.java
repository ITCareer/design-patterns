package com.quasigroup.education.creational.frameworks.builder;

import lombok.Builder;
import org.apache.logging.log4j.util.Strings;

@Builder
class DataSource {
    private String host;
    private String type;
    private String driver;
    private String schemaName;
    private Integer portNumber;

    public void register() throws ClassNotFoundException {
        Class.forName(driver);
    }

    public String getUrl() {
        String location = (Strings.isNotEmpty(host) && Strings.isNotEmpty(portNumber.toString())) ? "//"+host+":"+portNumber: "mem";
        return "jdbc:"+type+":"+location+"/"+schemaName;
    }
}
