package com.quasigroup.education.creational.basic.factories;

import java.sql.Connection;
import java.sql.SQLException;

public interface AbstractFactory {
    Connection createDBConnection() throws SQLException;
}
