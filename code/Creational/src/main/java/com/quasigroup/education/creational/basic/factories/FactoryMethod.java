package com.quasigroup.education.creational.basic.factories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//问题
//:如何安装需求切换不同的数据库连接
interface FactoryMethod {
    String PG = "";
    String H2 = "H2";

    //通过参数，生成不同内容的对象，但是有可能每个对象都是同一个类型，或者其子类型
    default Connection createDBConnection(String dbType) throws SQLException {
        switch (dbType) {
            case PG:
                return DriverManager.getConnection("jdbc:postgresql://localhost:5432/test", "db", "admin");
            case H2:
                return DriverManager.getConnection("jdbc:h2:mem:test", "user", "password");
            default:
                throw new IllegalArgumentException("Unexpected Data base type: " + dbType);
        }
    }

}
