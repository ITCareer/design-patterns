package com.quasigroup.education.creational.basic.factories;

import java.sql.Connection;
import java.sql.SQLException;


final public class FactoryCaller {


    private AbstractFactory factory = new PGFactory();
   // private AbstractFactory factory = new PGFactory();

    public void useFactoryMethod() throws SQLException {
        //Connection connection1 = FactoryMethod.createDBConnection(FactoryMethod.H2);
        Connection connection2 = FactoryMethod.createDBConnection("PG");
    }

    public void useAbstractory() throws SQLException {
        Connection connection1 = new PGFactory().createDBConnection();
    }

//    public void useCombined() throws SQLException {
//        Connection connection1 = FactoryMethod.createDBConnectionByAbstractFactory(FactoryMethod.H2F);
//        Connection connection2 = FactoryMethod.createDBConnectionByAbstractFactory(FactoryMethod.PGF);
//    }

}
