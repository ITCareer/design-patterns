package com.quasigroup.education.creational.frameworks.factories;

import java.sql.Connection;
import java.sql.SQLException;
//使用抽象工厂模式建立数据库连接
//目的和factoryMethod一致
interface AbstractFactory {
    Connection createDBConnection() throws SQLException;
}
