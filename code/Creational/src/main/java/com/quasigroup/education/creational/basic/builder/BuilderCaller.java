package com.quasigroup.education.creational.basic.builder;

final public class BuilderCaller {
    public DataSource buildH2Basic() {
        return new DataSource("org.h2.Drive", "H2","test");
    }

    public DataSource buildPGBasic() {
        return new DataSource(
                "org.postgresql.Driver",
                "postgresql",
                "testing",
                "localhost",
                5432);
    }

    public DataSource buildH2Builder() {
        return new DataSource.Builder("org.h2.Drive", "H2", "test").build();
    }

    public DataSource buildPGBuilder() {
        return new DataSource.Builder("org.postgresql.Driver", "postgresql","test")
                .atHost("localhost")
                .withPort(5432)
                .build();
    }
}
