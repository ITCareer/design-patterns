package com.quasigroup.education.creational.frameworks.factories;


import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class H2Factory implements AbstractFactory {

    @Override
    public Connection createDBConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:oracle:thin:@<hostname>:<port num>:<DB name>", "user", "password");

    }
}
