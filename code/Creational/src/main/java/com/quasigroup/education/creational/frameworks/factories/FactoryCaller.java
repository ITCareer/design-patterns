package com.quasigroup.education.creational.frameworks.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;

@Component
final public class FactoryCaller {

    @Autowired
    private AbstractFactory factory;

    public void useFactoryMethod() throws SQLException {
        Connection connection1 = FactoryMethod.createDBConnection(FactoryMethod.H2);
        Connection connection2 = FactoryMethod.createDBConnection(FactoryMethod.PG);
    }

    public void useAbstractFactory() throws SQLException {
        Connection connection = factory.createDBConnection();
    }

    public void useCombined() throws SQLException {
        Connection connection1 = FactoryMethod.createDBConnectionByAbstractFactory(factory);
    }

}
