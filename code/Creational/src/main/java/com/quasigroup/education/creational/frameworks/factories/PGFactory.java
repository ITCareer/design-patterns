package com.quasigroup.education.creational.frameworks.factories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PGFactory implements AbstractFactory {

    @Override
    public Connection createDBConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:oracle:thin:@<hostname>:<port num>:<DB name>", "user", "password");

    }
}
