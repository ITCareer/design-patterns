package com.quasigroup.education.creational.scala

import java.sql.Connection

package object dependence {

  final class FactoryMethodDI(var factory: factories.AbstractFactory) {
    def getConnection: Connection = factory.createDBConnection
  }

}
