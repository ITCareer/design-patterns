package com.quasigroup.education.creational.scala

import java.sql.{Connection, DriverManager}

package object factories {
  val PG = "Postgres"
  val H2 = "H2"
  def createDBConnection(dbType: String): Connection = dbType match {
    case PG =>
      DriverManager.getConnection("jdbc:postgres://localhost:5432", "user", "password")
    case H2 =>
      DriverManager.getConnection("jdbc:h2:mem", "user", "password")
    case _ =>
      throw new IllegalStateException("Unexpected value: " + dbType)
  }

  trait AbstractFactory {
    def createDBConnection: Connection
  }

  class H2Factory extends AbstractFactory {

    override def createDBConnection: Connection =
      DriverManager.getConnection("jdbc:h2:mem", "user", "password")
  }

  class PGFactory extends AbstractFactory {

    override def createDBConnection: Connection =
      DriverManager.getConnection("jdbc:postgres://localhost:5432", "user", "password")
  }

}
