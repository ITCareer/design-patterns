package com.quasigroup.education.creational.scala

package object instance {

  trait DatabaseType {
    def name: String
  }
  object H2 extends DatabaseType {
    def name: String = "h2"
  }
  object PG extends DatabaseType {
    def name: String = "postgres"
  }

}
