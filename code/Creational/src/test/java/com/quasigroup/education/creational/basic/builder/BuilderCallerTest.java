package com.quasigroup.education.creational.basic.builder;

import org.junit.Assert;
import org.junit.Test;

public class BuilderCallerTest {
    BuilderCaller builderCaller = new BuilderCaller();

    @Test
    public void testBuildH2Basic() throws Exception {
        DataSource result = builderCaller.buildH2Basic();
        Assert.assertEquals("jdbc:H2:mem/test", result.getUrl());
    }

    @Test
    public void testBuildPGBasic() throws Exception {
        DataSource result = builderCaller.buildPGBasic();
        Assert.assertEquals("jdbc:postgresql://localhost:5432/testing", result.getUrl());
    }

    @Test
    public void testBuildH2Builder() throws Exception {
        DataSource result = builderCaller.buildH2Builder();
        Assert.assertEquals("jdbc:H2:mem/test", result.getUrl());
    }

    @Test
    public void testBuildPGBuilder() throws Exception {
        DataSource result = builderCaller.buildPGBuilder();
        Assert.assertEquals("jdbc:postgresql://localhost:5432/test", result.getUrl());
    }
}
