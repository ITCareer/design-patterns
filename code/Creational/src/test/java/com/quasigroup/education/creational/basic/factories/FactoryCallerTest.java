package com.quasigroup.education.creational.basic.factories;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class FactoryCallerTest {
    @Mock
    AbstractFactory factory;
    @InjectMocks
    FactoryCaller factoryCaller;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUseFactoryMethod() throws Exception {
        Class.forName("org.postgresql.Driver");
        factoryCaller.useFactoryMethod();
    }

    @Test
    public void testUseAbstractory() throws Exception {
        Class.forName("org.postgresql.Driver");
        when(factory.createDBConnection()).thenReturn(null);

        factoryCaller.useAbstractory();
    }

    @Test
    public void testUseCombined() throws Exception {
        Class.forName("org.postgresql.Driver");
        factoryCaller.useCombined();
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme