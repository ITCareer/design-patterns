package com.quasigroup.education.creational.basic.dependence;

import com.quasigroup.education.creational.basic.factories.AbstractFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class FactoryMethodDITest {
    @Mock
    AbstractFactory factory;
    @InjectMocks
    FactoryMethodDI factoryMethodDI;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUseAbstractory() throws Exception {
        Class.forName("org.postgresql.Driver");
        when(factory.createDBConnection()).thenReturn(null);

        factoryMethodDI.getConnection();
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme