package com.quasigroup.education.creational.scala

package object builder {
    final case class DataSource(driver: String,
                                sourceType: String,
                                host: String = "",
                                schemaName: String = "test",
                                portNumber: Option[Integer] = None) {
      def register(): Unit = {
        Class.forName(driver)
      }

      private val location = if (host.isEmpty) "mem" else portNumber.fold(s"//$host")(port => s"//$host:$port")

      def getUrl: String = s"jdbc:$sourceType:$location/$schemaName"

    }

    val h2 = DataSource(driver = "org.h2.Drive",sourceType = "h2")

    val pg = DataSource(driver = "org.postgresql.Driver",
                        sourceType = "postgres",
                        host = "localhost",
                        portNumber = Some(5432),
                        schemaName = "testing")

}
