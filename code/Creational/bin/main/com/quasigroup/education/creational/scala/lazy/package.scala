package com.quasigroup.education.creational.scala

package object `lazy` {
   object LazyInitializedSingleton {
     println("singleton init")
   }

   println("start")

}
